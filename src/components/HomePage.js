import React, { useEffect, useState } from 'react'
import { addEmpDetails, getEmployeeById } from './../services/api'
import './HomePage.css'

function HomePage() {
    // state for initial fixed columns
    const [fixedColumns, setFixedColumns] = useState([

        "Emp ID", "Gender", "Date Of Joining", "DOB", "Emp Department", "Emp Designation"
    ])
    // state for input field values if present in database
    let [inputValues, setInputValues] = useState([""]);
    // state for number of columns present 
    const [columnLength, setColumnLength] = useState(6);
    // // state for plus popup
    // const [show, setShow] = useState(false);
    // //state for employee exist popup
    // const [empPopUp, setEmpPopUp] = useState(false);
    //state to get the index of plus clicked
    const [clickedIndex, setClickedIndex] = useState(0);
    //state to store the name of new column added
    const [newcolumn, setNewColumn] = useState("");
    //state to store the search result
    const [searchDetails, setSearchDetails] = useState({});
    //state to store the employee name
    const [empName, setEmpName] = useState("");
    //state to store the color for verification field
    const [color, setColor] = useState("")
    //state to store the imput field values 
    const [formData, updateFormData] = useState({});
    //state to store number of fields available 
    const [totalField, setTotalField] = useState(fixedColumns.length + 1);
    //state to set the button name
    const [btnName, setBtnName] = useState("Submit")
    //state to manage counter
    const [counter, setCounter] = useState()
    //state to manage notification
    const [notification, setNotification] = useState()
    //state for notification class
    const [notifyClass, setNotifyClass] = useState("")
    //state for notification background
    const [notifyCss, setNotifyCss] = useState({
        "bgcolor": "green",
        "color": "white"
    })
    const [firstmodel, setFirstmodel] = useState("none")
    const [secondmodel, setSecondmodel] = useState("none")
    // function to handle popup close operation
    const handleClose = () => {
        setFirstmodel("none");
        setSecondmodel("none");
        // setShow(false);
        //setEmpPopUp(false);
    };

    //function for add column popup
    const handleShow = () => {
        setFirstmodel("block");
        // setShow(true);
    }


    //function to add the new column
    const myfunc = () => {
        let newElement = [...fixedColumns];
        let newVal = [...inputValues];
        let validateCopy = 0;
        console.log("initial", validateCopy)
        console.log("new element", newElement)
        console.log("new column", newcolumn)
        for (let y = 0; y < newElement.length; y++) {
            if (newElement[y] === newcolumn) {
                validateCopy = validateCopy + 1;
            }
        }
        console.log("final", validateCopy);
        if (validateCopy === 0) {
            for (let i = 0; i < newElement.length; i++) {
                if (i === clickedIndex) {
                    for (let x = newElement.length - 1; x > i; x--) {
                        newElement[x + 1] = newElement[x];
                        newVal[x + 1] = newVal[x];
                    }
                }
            }

            newElement[clickedIndex + 1] = newcolumn;
            newVal[clickedIndex + 1] = "";
            setFixedColumns(newElement);
            setInputValues(newVal);
            setColumnLength(columnLength + 1);
            setFirstmodel("none");
            // setShow(false);
            setTotalField(totalField + 1);
        } else {
            setNotification("Column Already Exists ")
            setNotifyClass("show");
            setNotifyCss({
                "bgcolor": "red",
                "color": "white"
            })
        }

    }


    // function to set the form data
    const handleChange = (e, i) => {
        updateFormData({
            ...formData,

            // Trimming any whitespace
            [e.target.name]: e.target.value.trim()
        });
        if (i === "name") {
            setEmpName(e.target.value);
        } else {
            inputValues[i] = e.target.value;
        }
    };
    //console.log("formdata:", formData)
    // function to handle submit request
    const handleSubmit = (e) => {
        e.preventDefault()


        //Mapping Data

        let dataToSend = {
            "empId": formData["Emp ID"] ? formData["Emp ID"] : "",
            "empName": formData.name ? formData.name : "",
            // "gender": formData["Gender"] ? formData["Gender"] : "",
            // "DOJ": formData["Date Of Joining"] ? formData["Date Of Joining"] : "",
            // "DOB": formData["DOB"] ? formData["DOB"] : "",
            // "empDepartment": formData["Emp Department"] ? formData["Emp Department"] : "",
            // "empDesignation": formData["Emp Designation"] ? formData["Emp Designation"] : "",
            "additionalField": [{}]
        }

        // let lenKeys = Object.keys(formData);
        // let lenValues = Object.values(formData);
        let x = 0;
        for (let i = 0; i < fixedColumns.length; i++) {
            if (fixedColumns[i] === "Emp ID" || fixedColumns[i] === "name") {
                console.log("test");
            } else {
                dataToSend.additionalField[x] = {
                    [fixedColumns[i]]: inputValues[i]
                }
                x++;
            }
        }
        console.log("after Map", dataToSend);
        // ... API call


        if (dataToSend.empId === "") {
            setNotification("Please Enter Employee ID");
            setNotifyClass("show");
            setNotifyCss({
                "bgcolor": "red",
                "color": "white"
            })
        } else if (dataToSend.empName === "") {
            setNotification("Please Enter Employee Name")
            setNotifyClass("show");
            setNotifyCss({
                "bgcolor": "red",
                "color": "white"
            })
        }
        else {
            addEmpDetails(dataToSend)
                .then((response) => {
                    console.log("success");
                    setFixedColumns([
                        "Emp ID", "Gender", "Date Of Joining", "DOB", "Emp Department", "Emp Designation"
                    ]);
                    setColumnLength(6);
                    setInputValues([""]);
                    updateFormData({});
                    setTotalField(7);
                    setEmpName("");
                    setBtnName("Submit");
                    setNotification("Details Succesfully Added")
                    setNotifyClass("show");
                    setNotifyCss({
                        "bgcolor": "green",
                        "color": "white"
                    })
                })
        }
    };


    useEffect(() => {
        setTimeout(() => {
            if (notifyClass !== "") {
                setNotifyClass("");
            }
        }, 3000);
    }, [notifyClass])



    //console.log("columnLength", columnLength)
    //function to handle employee exist popup
    const handleEmpPopUp = async (e, i) => {


        if (e !== "") {


            let details = await getEmployeeById(e);
            //console.log("details", details.data)
            // if(details.data.)
            if (details.data.length !== 0) {
                setSecondmodel("block");
                // setEmpPopUp(true);
                setSearchDetails(details.data);
                setCounter(1);
            } else {
                if (counter === 1) {
                    setFixedColumns([
                        "Emp ID", "Gender", "Date Of Joining", "DOB", "Emp Department", "Emp Designation"
                    ]);
                    setColumnLength(6);
                    setInputValues([""]);
                    updateFormData({
                        "Emp ID": e
                    });
                    setInputValues([e])
                    setTotalField(7);
                    setEmpName("");
                    setBtnName("Submit");
                }
                setCounter(0);
            }
            //console.log(details.data);
        }

    }
    // console.log("Input Values", inputValues);
    // function to fill the exixting employee data 
    const fillData = () => {
        setEmpName(searchDetails['Emp Name']);
        let dataFromDB = {
            "name": searchDetails['Emp Name']
        };
        delete searchDetails['Emp Name'];
        delete searchDetails.id;
        const columnKeyArray = Object.keys(searchDetails)
        const columValueArray = Object.values(searchDetails)
        setFixedColumns(columnKeyArray);
        setColumnLength(columnKeyArray.length);
        setInputValues(columValueArray);
        //console.log("testing", searchDetails)
        setSecondmodel("none");
        // setEmpPopUp(false);


        for (let i = 0; i < columnKeyArray.length; i++) {
            dataFromDB = {
                ...dataFromDB,
                [columnKeyArray[i]]: columValueArray[i]
            }
        }
        updateFormData(dataFromDB);
        setTotalField(columnKeyArray.length + 1);
        setBtnName("Update")

    }
    const positionHandler = (e) => {
        setClickedIndex(parseInt(e.target.value));
        // console.log("position", e.target.value)
    }

    //function to set color code to verification field
    useEffect(() => {
        const addVerification = () => {

            const completedFields = Object.values(formData)
            let myarray = []
            let x = 0;
            for (let i = 0; i < completedFields.length; i++) {
                if (completedFields[i] !== "") {
                    myarray[x] = completedFields[i];
                    x++;
                }
            }
            // console.log("param1", myarray.length);
            // console.log("param2", totalField);
            if (totalField === myarray.length) {
                setColor("green");
            } else if (myarray.length === 0) {
                setColor("red")
            } else {
                setColor("yellow")
            }

        }
        addVerification();
    }, [formData, totalField])

    // console.log("total input", fixedColumns,inputValues)
    //console.log("Formdata", formData)

    return (
        <div className="custom-container">
            <div className='custom-row forstyle'>
                <div className='custom-md-12' style={{ textAlign: "center", padding: "10px 10px" }}>
                    <span className="btn-shine">Employee Information Table</span>
                </div>

            </div>
            {/* <form name="frm" > */}
            <div className='custom-row' style={{ padding: "20px", bacgroundColor: "white", boxShadow: "0px 3px 20px 5px rgb(0 0 0 / 7%) ", borderRadius: "12px" }}>

                <div className='custom-md-12 forstyle'>
                    <div className='custom-row'>
                        <div className='custom-md-2 headingClass' >
                            <label>Emp Name*</label>
                        </div>
                        <div className='custom-md-10' style={{ paddingRight: "5px" }}>
                            <input type="text" className='custom-form-control' value={empName} name="name" onChange={(e) => { handleChange(e, "name") }}></input>
                        </div>
                    </div>
                </div>
                {
                    fixedColumns.map((item, i) => (
                        <div className='custom-md-6 forstyle' key={i}>
                            <div className='custom-row'>
                                <div className='custom-md-4 headingClass'>
                                    <label>{item === "Emp ID" ? item + "*" : item}</label>
                                </div>
                                <div className='custom-md-8' style={{ paddingRight: "5px" }}>

                                    {item === "Emp ID" ?
                                        <input type="text" className='custom-form-control' value={inputValues[i] === undefined ? "" : inputValues[i]} name={item} onChange={(e) => { handleChange(e, i) }} onBlur={(e) => handleEmpPopUp(e.target.value, i)}></input> :
                                        item === "Date Of Joining" || item === "DOB" ? <input type="date" className='custom-form-control' value={inputValues[i] === undefined ? "" : inputValues[i]} name={item} onChange={(e) => { handleChange(e, i) }}></input> :
                                            <input type="text" className='custom-form-control' value={inputValues[i] === undefined ? "" : inputValues[i]} name={item} onChange={(e) => { handleChange(e, i) }}></input>
                                    }


                                </div>
                            </div>
                        </div>
                    ))}


                <div className={columnLength % 2 === 0 ? "custom-md-12 forstyle" : "custom-md-6 forstyle"}>
                    <div className='custom-row'>
                        <div className={columnLength % 2 === 0 ? "custom-md-2 headingClass" : "custom-md-4 headingClass"}  >
                            <label>Emp Verification</label>
                        </div>
                        <div className={columnLength % 2 === 0 ? "custom-md-9 " : "custom-md-7 "} style={{ paddingRight: "5px" }}>
                            <input type="text" className='custom-form-control' name="empverification" style={{ color: "white", backgroundColor: color }} value={color === "red" ? "Empty Form" : color === "yellow" ? "Partially Filled" : "Form Completed"} readOnly></input>
                        </div>
                        <div className="custom-md-1" style={{ paddingRight: "5px" }}>
                            <button className='custom-btn custom-btn-success' onClick={() => handleShow()} style={{ width: "100%" }}>+</button>
                        </div>
                    </div>
                </div>
                <div className='custom-md-12' style={{ textAlign: "right", paddingRight: "5px" }}>
                    <button type="submit" className='custom-btn custom-btn-primary' onClick={handleSubmit} style={{ width: "200px" }}>{btnName}</button>
                </div>
            </div>
            {/* </form> */}
            {/* Model popup 1 */}
            <div className='model-popup' style={{ display: firstmodel }}>
                <div className='modal-bg'></div>
                <div className="snackbar-model">

                    <div className='model-header'>
                        <h5>Add A New Column</h5>

                    </div>

                    <div className='model-body'>
                        <h6>Enter New Column Name</h6>
                        <div><input type="text" name="newcolumn" className='custom-form-control' onChange={(e) => setNewColumn(e.target.value)}></input></div>
                        <h6>Select Postion of New Column</h6>
                        <div>
                            <select name="columns" className='custom-form-control' onChange={(e) => positionHandler(e)}>
                                {
                                    fixedColumns.map((item, i) => (
                                        <option value={i} key={i}>{item}</option>
                                    )
                                    )}
                            </select>
                        </div>
                    </div>

                    <div className='model-footer'>

                        <button className='custom-btn custom-btn-secondary' onClick={handleClose}>
                            Close
                        </button>

                        <button className='custom-btn custom-btn-primary' style={{ marginLeft: "5px" }} onClick={myfunc}>
                            Save Changes
                        </button>
                    </div>


                </div>
            </div>
            {/* model popup 2 */}
            <div className='model-popup' style={{ display: secondmodel }}>
                <div className='modal-bg'></div>
                <div className="snackbar-model">

                    <div className='model-header'>
                        <h5>Emp ID Found</h5>

                    </div>

                    <div className='model-body'>
                        <h6>Employee ID already Exists in our database</h6>
                        <div>Click Proceed to load the existing data</div>
                    </div>

                    <div className='model-footer'>

                        <button className='custom-btn custom-btn-secondary' onClick={handleClose}>
                            Close
                        </button>

                        <button className='custom-btn custom-btn-primary' style={{ marginLeft: "5px" }} onClick={fillData}>
                            Proceed
                        </button>
                    </div>


                </div>
            </div>

            {/* <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add A New Column</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h6>Enter New Column Name</h6>
                        <div><input type="text" name="newcolumn" className='custom-form-control' onChange={(e) => setNewColumn(e.target.value)}></input></div>
                        <h6>Select Postion of New Column</h6>
                        <div>
                            <select name="columns" className='custom-form-control' onChange={(e) => positionHandler(e)}>
                                {
                                    fixedColumns.map((item, i) => (
                                        <option value={i} key={i}>{item}</option>
                                    )
                                    )}
                            </select>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={myfunc}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal> */}


            {/* notification */}
            <div>
                <div id="snackbar" style={{ backgroundColor: notifyCss.bgcolor, color: notifyCss.color }} className={notifyClass}>{notification}</div>
                {/* <Modal show={empPopUp} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Emp ID Found</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{ textAlign: "center" }}>
                        <h6>Employee ID already Exists in our database</h6>
                        <div>Click Proceed to load the existing data</div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={fillData}>
                            Proceed
                        </Button>
                    </Modal.Footer>
                </Modal> */}

            </div>
        </div>

    )
}

export default HomePage