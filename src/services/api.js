import axios from "axios";

const MAINURL = "http://localhost:8080/"

 //const tempURL = 'http://localhost:45677/'

// Get Attendance by Student Id and Batch Id
export const addEmpDetails = async (body) => {
    return await axios.post(`${MAINURL}addEmployee`, body)
}

export const getEmployeeById= async(id) => {
    return await axios.get(`${MAINURL}getEmployee/${id}`)
}